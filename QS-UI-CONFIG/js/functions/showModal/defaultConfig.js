module.exports = function () {
	return {
		content: '',
		title: '',
		showCancel: true,
		cancelText: '取消',
		cancelColor: '',
		confirmText: '确定',
		confirmColor: uni.$qs.store.getters['theme/theme'].primary,
		success: () => {},
		fail: () => {},
		complete: () => {}
	}
}