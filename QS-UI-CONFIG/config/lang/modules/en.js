module.exports = {
	'qs-des': 'We are all meteors for a while but who can shine like it',
	test: {
		test: 'test_1',
		modifyLang: 'Modify Lang'
	},
	router: {
		js: {
			multilang: 'Multilang',
			router: 'Router',
			testRouterOpen: 'TestRouterOpen',
			testRouterLogin: 'TestRouterLogin',
			getSys: 'SystemInfo',
			interaction: 'Interaction',
			request: 'Request',
			handleAddress: 'Address Resolution',
			countDown: 'Count Down',
			theme: 'Theme',
			refresh: 'Refresh',
			query: 'Query'
		},
		components: {
			QSAnimation: 'QS-Animation Animation',
			QSBackTop: 'QS-BackTop Backtop',
			QSBadge: 'QS-Badge Badge',
			QSButton: 'QS-Button Button',
			QSTabs: 'QS-Tabs Tabs',
			QSticky: 'QS-ticky sticky',
			QSComponentReading: 'QS-ComponentReading',
			QSForm: 'QS-Form Form',
			QSDynamicForm: 'QS-DynamicForm DynamicForm',
			QSGrids: 'QS-Grids Grids',
			QSCanvas: 'QS-Canvas Cavnas',
			QSSuperComponent: 'QS-SuperComponent JSON dynamic rendering components',
			compClassAndcompStyle: 'compClassAndcompStyle',
			pageContext: 'pageContext',
			emitQuery: 'emitQuery'
		},
		nvueComponents: {
			// nvue
			QSAnimation_n: 'QS-Animation Animation',
			QSBackTop_n: 'QS-BackTop Backtop',
			QSBadge_n: 'QS-Badge Badge',
			QSButton_n: 'QS-Button Button',
			QSTabs_n: 'QS-Tabs Tabs',
			QSForm_n: 'QS-Form Form',
			QSDynamicForm_n: 'QS-DynamicForm DynamicForm',
			QSGrids_n: 'QS-Grids Grids'
		},
		template: {
			home_1: 'home-1 Home-1'
		}
	},
	tabbar: {
		js: 'JS',
		components: 'Components',
		template: 'Template',
	}
}