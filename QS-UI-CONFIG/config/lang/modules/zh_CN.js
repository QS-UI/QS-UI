module.exports = {
	'qs-des': '我们都如流星短暂 但谁能像它闪耀',
	test: {
		test:'测试多语言',
		modifyLang: '改变语言'
	},
	router: {
		js: {
			multilang: '多语言',
			router: '路由',
			testRouterOpen: '测试路由跳转',
			testRouterLogin: '测试路由跳转(校验登录)',
			getSys: '系统信息',
			interaction: '交互反馈',
			request: '请求接口',
			handleAddress: '地址解析',
			countDown: '倒计时',
			theme: '主题',
			refresh: '刷新',
			query: '布局信息'
		},
		components: {
			QSAnimation: 'QS-Animation 动画',
			QSBackTop: 'QS-BackTop 返回顶部',
			QSBadge: 'QS-Badge 标记',
			QSButton: 'QS-Button 按钮',
			QSTabs: 'QS-Tabs 标签',
			QSticky: 'QS-ticky 黏贴',
			QSComponentReading: 'QS组件须知',
			QSForm: 'QS-Form 表单',
			QSDynamicForm: 'QS-DynamicForm 动态表单',
			QSGrids: 'QS-Grids 宫格组件',
			QSCanvas: 'QS-Canvas 画布',
			QSSuperComponent: 'QS-SuperComponent JSON动态渲染组件',
			compClassAndcompStyle: 'compClass与compStyle',
			pageContext: 'pageContext',
			emitQuery: 'emitQuery'
		},
		nvueComponents: {
			// nvue
			QSAnimation_n: 'QS-Animation 动画',
			QSBackTop_n: 'QS-BackTop 返回顶部',
			QSBadge_n: 'QS-Badge 标记',
			QSButton_n: 'QS-Button 按钮',
			QSTabs_n: 'QS-Tabs 标签',
			QSForm_n: 'QS-Form 表单',
			QSDynamicForm_n: 'QS-DynamicForm 动态表单',
			QSGrids_n: 'QS-Grids 宫格组件'
		},
		template: {
			home_1: 'home-1 首页-1'
		}
	},
	tabbar: {
		js: 'JS',
		components: '组件',
		template: '模板',
	}
}