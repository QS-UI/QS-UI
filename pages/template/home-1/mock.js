module.exports = {
	navbar1Items: [{
		type: 'text',
		text: 'QS-UI',
		fontSize: '18px',
		weight: '600',
		color: '',
		width: 100
	}],
	navbar2Items: [{
			type: 'search',
			text: '搜索一下',
			flex: 1,
			height: '31px',
			fontSize: '15px',
			fontWeight: 600,
			color: '#666666',
			padding: '0 18rpx',
			hasIcon: false,
			backgroundColor: '#ffffff',
			borderBottomLeftRadius: '60px',
			borderBottomRightRadius: '60px',
			borderTopLeftRadius: '60px',
			borderTopRightRadius: '60px',
			layout: 'left'
		},
		{
			type: 'icon',
			icon: 'service',
			iconSize: '24px',
			marginLeft: '30px',
			iconColor: '#ffffff',
		},
		{
			type: 'icon',
			icon: 'scan',
			iconSize: '24px',
			marginLeft: '30px',
			iconColor: '#ffffff',
		}
	],
	topBgImage: {
		src: '',
		// src: 'https://imgsa.baidu.com/forum/w%3D580/sign=93d4278705f3d7ca0cf63f7ec21ebe3c/24deb730e924b899a2f7198167061d95087bf6a2.jpg',
		style: {
			backgroundColor: '#1C8DE3',
			height: '500px'
		}
	},
	topSwiperData: [{
			path: '',
			backgroundColor: '#1C8DE3'
		},
		{
			path: '',
			backgroundColor: '#2fe3a7'
		},
		{
			path: '',
			backgroundColor: '#a36fe3'
		},
		{
			path: '',
			backgroundColor: '#e3a43d'
		},
		{
			path: '',
			backgroundColor: '#e3a8a8'
		},
		{
			path: '',
			backgroundColor: '#aaaa7f'
	}],
	sort2TabsProps: {
		columnBetweenActiveBgColor: '#F52F50',
		columnBetweenLineColor: '#f2f2f2'
	},
	sort1Tabs: [
		{name: '推荐'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
		{name: '分类'},
	],
	sort2Tabs: [{
			name: '全部',
			botTitle: '猜你喜欢',
			list: []
		},
		{
			name: '开抢预告',
			botTitle: '抢购进行中',
			list: []
		},
		{
			name: '热门',
			botTitle: '超值购',
			list: []
		},
		{
			name: '排行榜',
			botTitle: '大家都在买',
			list: []
		}
	],
	res: {
		elements: [{
				type: 'view',
				attributes: {
					compStyle: 'background-color: #ffffff;'
				},
				res: {
					elements: [{
							type: 'QS-P',
							attributes: {
								height: '8rpx'
							}
						},
						{
							type: 'QS-P',
							attributes: {
								height: '26rpx'
							}
						},
						{
							type: 'QS-Grids',
							attributes: {
								row: 4,
								gridTextSize: '26rpx',
								gridMinHeight: '120rpx',
								gridPadding: '0 15rpx',
								iconMinHeight: '80rpx',
								gridsData: [{
										img: '/static/logo.png',
										height: '80rpx',
										width: '80rpx',
										text: '停车缴费',
										event: {
											type: 'navigateTo',
											path: 'xxx',
										}
									},
									{
										img: '/static/logo.png',
										height: '80rpx',
										width: '80rpx',
										text: '积分商城',
										event: {
											type: 'navigateTo',
											path: 'xxx',
										}
									},
									{
										img: '/static/logo.png',
										height: '80rpx',
										width: '80rpx',
										text: '品牌导览',
										event: {
											type: 'navigateTo',
											path: 'xxx',
										}
									},
									{
										img: '/static/logo.png',
										height: '80rpx',
										width: '80rpx',
										text: '电影专享',
										event: {
											type: 'navigateTo',
											path: 'xxx',
										}
									},
								]
							}
						},
						{
							type: 'QS-P',
							attributes: {
								height: '26rpx'
							}
						},
					]
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '1px'
				}
			},
			{
				type: 'verticalSwiper-2',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					leftImageConfig: {
						src: '/static/logo.png',
						event: {
							type: 'navigateTo',
							path: 'xxxx'
						}
					},
					rightSwiper: {
						autoplay: true,
						interval: 3000,
						list: [
							{
								compStyle: 'height: 100%;',
								attributes: {
									text: '点击查看近期1'
								},
								event: {
									type: 'navigateTo',
									path: 'xxxx'
								}
							},
							{
								compStyle: 'height: 100%;',
								attributes: {
									text: '点击查看近期1'
								},
								event: {
									type: 'navigateTo',
									path: 'xxxx'
								}
							},
						]
					},
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '20rpx'
				}
			},
			{
				type: 'view',
				attributes: {
					compStyle: 'background-color: #ffffff;padding: 30rpx 25rpx;'
				},
				res: {
					elements: [{
							type: 'view',
							res: {
								compClass: 'flex_row',
								elements: [{
										type: 'view',
										attributes: {
											compStyle: 'flex: 1;',
											compClass: 'flex_row_c_none'
										},
										res: {
											elements: [{
												type: 'flashSale-1',
												compClass: 'flex_1',
												compStyle: 'margin-bottom: 25rpx;',
												attributes: {
													titleConfig: {
														title: '限时购'
													},
													countDown: +new Date() + 1000 * 60 * 60 * 3,
													products: [{
														img: '',
														discount: '4.9'
													}, {
														img: '',
														discount: '6.6'
													}]
												}
											}],
										},
									},
									{
										type: 'QS-P',
										attributes: {
											height: '100%',
											width: '1px',
											backgroundColor: '#f7f7f7'
										}
									},
									{
										type: 'view',
										attributes: {
											compStyle: 'flex: 1;',
											compClass: 'flex_row_c_none'
										},
										res: {
											elements: [{
												type: 'top-1',
												compClass: 'flex_1',
												compStyle: 'margin-bottom: 25rpx;',
												attributes: {
													titleConfig: {
														title: '排行榜'
													},
													products: [{
														img: '',
														num: '102'
													}, {
														img: '',
														num: '44'
													}]
												}
											}],
										},
									},
								]
							}
						},
						{
							type: 'QS-P',
							attributes: {
								height: '1px',
								width: '100%',
								backgroundColor: '#f7f7f7'
							}
						},
						{
							type: 'view',
							res: {
								compClass: 'flex_row',
								elements: [{
										type: 'view',
										attributes: {
											compStyle: 'flex: 1;',
											compClass: 'flex_row_c_none'
										},
										res: {
											elements: [{
												type: 'bargainBuy-1',
												compClass: 'flex_1',
												compStyle: 'margin-top: 25rpx;',
												attributes: {
													titleConfig: {
														title: '超值购'
													},
													products: [{
														img: '',
														name: '这是一条标题这是一条标题',
														price: '600'
													}, {
														img: '',
														name: '这是一条标题这是一条标题',
														price: '600'
													}]
												}
											}],
										},
									},
									{
										type: 'QS-P',
										attributes: {
											height: '100%',
											width: '1px',
											backgroundColor: '#f7f7f7'
										}
									},
									{
										type: 'view',
										attributes: {
											compStyle: 'flex: 1;',
											compClass: 'flex_row_c_none'
										},
										res: {
											elements: [{
												type: 'ventureGroup-1',
												compClass: 'flex_1',
												compStyle: 'margin-top: 25rpx;',
												attributes: {
													titleConfig: {
														title: '大家都在拼'
													},
													userList: ['', '', '', ''],
													products: [{
														img: '',
														name: '这是一条标题这是一条标题',
														price: '600'
													}, {
														img: '',
														name: '这是一条标题这是一条标题',
														price: '600'
													}]
												}
											}],
										},
									},
								]
							}
						},
					]
				}
			},
			{
				type: 'view',
				attributes: {
					compStyle: 'padding: 25rpx;background-color: #ffffff;',
				},
				res: {
					elements: [{
						type: 'brandActivities-1',
						compStyle: 'background-color: #F7F7F7;border-radius: 10rpx',
						attributes: {
							titleConfig: {
								title: '品牌活动'
							},
							list: ['', '', '', '', '', '']
						}
					}]
				}
			},
			{
				type: 'mode-1',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					titleConfig: {
						title: 'mode-1',
					},
					list: ['', '', '', '', '', '']
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '20rpx'
				}
			},
			{
				type: 'mode-2',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					titleConfig: {
						title: 'mode-2',
					},
					list: ['']
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '20rpx'
				}
			},
			{
				type: 'mode-3',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					titleConfig: {
						title: 'mode-3',
					},
					list: ['', '']
				}
			},
			{
				type: 'mode-4',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					topImageConfig: {
						height: '446rpx'
					},
					botContentBgColor: '#ffffff',
					list: ['', '', '', '', '', '']
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '20rpx'
				}
			},
			{
				type: 'mode-5',
				compStyle: 'background-color: #ffffff;',
				attributes: {
					titleConfig: {
						title: 'mode-5',
					},
					list: ['', '', '', '', '', '']
				}
			},
			{
				type: 'QS-P',
				attributes: {
					height: '20rpx'
				}
			},
		]
	}

}
