module.exports = function ({ componentType = '' } = {}) {
	return {
		name: componentType,
		inject: {
			QSSuperComponent: {
				default: ()=> null
			}
		},
		methods: {
			click() {
				this.QSSuperComponent?.$emit('click', componentType, ...arguments);
			}
		}
	}
}