import classObj2String from '../js/functions/classObj2String.js';
import classString2Array from '../js/functions/classString2Array.js';
import styleObj2String from '../js/functions/styleObj2String.js';
import rpxUnit2px from '../js/functions/rpxUnit2px.js';
import CONFIG from '@/QS-UI-CONFIG/index.js';
let props;
try{
	props = require('@/QS-UI-CONFIG/js/mixins/QS-Components-Mixin-defProps.js');
}catch(e){
	//TODO handle the exception
	props = {};
}
// const styleString2Object = function (str) {
// 	if(typeof str === 'string') {
// 		str = str.split(';').filter(i=>!!i).map(item=>{ const ite = item.split(':').map(it=>it.trim()); return { [ite[0]]: ite[1] }  });
// 	}
// 	return str;
// }
var id = 0;
module.exports = function({
	componentType,
	setContext,
	nodeName = ''
} = {}) {
	
	// #ifdef APP-NVUE
	const dom = weex.requireModule('dom');
	if(nodeName) {
		nodeName = nodeName.split(' ')
		nodeName = nodeName[nodeName.length - 1];
		nodeName = nodeName.split('#').join('').split('.').join('');
	}
	// #endif
	
	let _this;
	const nProps = {
		compClass: {
			type: Object,
			default: () => ({})
		},
		compStyle: {
			type: Object,
			default: () => ({})
		},
		fontSize: {
			type: [String, Number],
			default: ''
		},
		theme: {
			type: String,
			default: ''
		},
		autoEmitQuery: {
			type:  Boolean,
			default: false
		},
		autoEmitQueryDelay: {
			type: Number,
			default: 1000
		},
		...props
	}
	return {
		props: nProps,
		mixin: {
			name: componentType || '',
			// #ifndef MP-ALIPAY
			props: nProps,
			// #endif
			created() {
				_this = this;
				if (componentType && setContext) {
					// #ifndef APP-NVUE
					uni.$qs.pageRoots.setPageContext(this, componentType);
					// #endif
					// #ifdef APP-NVUE
					uni.$qs.pageRoots.setPageContext(_this, componentType);
					// #endif
				}
			},
			beforeDestroy() {
				if (componentType && setContext) uni.$qs.pageRoots.clearPageContext(this, componentType);
			},
			data() {
				return {
					componentId: `${componentType || 'QS-COMPONENT'}-${id++}`,
					baseFontSize: CONFIG.baseFontSize
				}
			},
			computed: {
				themes() {
					return uni.$qs.store.getters['theme/theme'];
				},
				getFontSize() {
					return rpxUnit2px(this.fontSize) || rpxUnit2px(this.baseFontSize);
				},
				getClass() {
					const c =
						`${classObj2String(this.compClass?.container)} ${classObj2String(this?.QS_nCompClass)}`;
					// #ifndef APP-NVUE
					return c
					// #endif
					// #ifdef APP-NVUE
					return classString2Array(c);
					// #endif
				},
				getStyle() {
					return `${styleObj2String(this.compStyle?.container)};${styleObj2String(this?.QS_nCompStyle)};`;
				}
			},
			mounted() {
				this.emitQuery();
			},
			methods: {
				emitQuery() {
					setTimeout(()=>{
						if (nodeName && this.autoEmitQuery) this.getQuery(data => {
							this.$emit('emitQuery', data);
						})
					}, this.autoEmitQueryDelay)
				},
				getRoot() {
					return this;
				},
				getQuery(cb) {
					// #ifdef APP-NVUE
					dom.getComponentRect(this.$refs[nodeName.replace(/{componentId}/g, this.componentId)], option => {
						// console.log('获取tabs布局信息成功: ' + JSON.stringify(option))
						if (cb && typeof cb === 'function') cb(option.size)
					})
					// #endif
					// #ifndef APP-NVUE
					let view;
					// #ifndef MP-BAIDU || MP-ALIPAY
					view = uni.createSelectorQuery().in(this);
					// #endif
					// #ifdef MP-BAIDU || MP-ALIPAY
					view = uni.createSelectorQuery();
					// #endif
					view.select(nodeName.replace(/{componentId}/g, this.componentId)).fields({
						size: true,
						rect: true
					})
					view.exec(res => {
						let data;
						if(Array.isArray(res)) data = res[0];
						else data = res;
						if (cb && typeof cb === 'function') cb(data)
						if(this.compMixin_emitQuery && typeof this.compMixin_emitQuery == 'function') this.compMixin_emitQuery(data);
					})
					// #endif
				}
			},
		}
	}
}
