module.exports = function (value, componentType) {
	switch(componentType) {
		case 'QS-Checkbox':
			return value || [];
			break;
		case 'QS-Switch':
			return !!value;
			break;
		default:
			return value;
			break;
	}
}