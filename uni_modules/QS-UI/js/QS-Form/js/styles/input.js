module.exports = function (isNvue) {
	return {
		container: {	//作用于最外层view标签的样式
			borderBottomWidth: '1px',
			borderBottomStyle: 'solid',
			borderBottomColor: '#adadad'
		},
		containerFocus: {	//当input聚焦时最外层view标签的样式
			borderBottomColor: '#000000'
		},
		input: {	//作用于input标签的样式
		
		},
		inputFocus: {	//当input聚焦时的样式
		
		}
	}
}