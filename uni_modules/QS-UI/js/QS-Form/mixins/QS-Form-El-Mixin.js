import {
	isArray
} from '../../baseUtil.js';
import getIsMountedMixin from './getIsMountedMixin.js';
import getParentOfName from '../../functions/getParentOfName.js';
import getDefaultValue from '../js/getDefaultValue.js';

const isMountedMixin = getIsMountedMixin();

const els = {};

module.exports = function({
	componentType
} = {}) {
	const props = {
		prop: {
			type: String,
			default: ''
		},
		value: {
			type: [String, Array, Boolean, Number, Object],
			default: ''
		},
		rules: {
			type: [Object, Array],
			default: () => null
		},
		disabled: {
			type: Boolean,
			default: false
		},
		dynamicForm: {
			type: Boolean,
			default: false
		}
	}
	return {
		props,
		mixin: {
			mixins: [isMountedMixin],
			// #ifndef MP-ALIPAY
			props,
			// #endif
			data() {
				return {
					val: getDefaultValue(this.value, componentType),
				}
			},
			// #ifndef MP-ALIPAY
			inject: ['QSForm', 'QSFormItem'],
			// #endif
			watch: {
				value(n, o) {
					this.val = n;
				},
				getRequired: {
					handler(n) {
						// #ifdef MP-ALIPAY
						if (this.dynamicForm) return;
						// #endif
						const QSFormItem = getParentOfName('QS-Form-Item', this);
						if (QSFormItem?.setElRequired) {
							QSFormItem.setElRequired(this, n);
						}
					},
					immediate: true
				}
			},
			computed: {
				getRequired() {
					if (this.isMounted) {
						const QSForm = getParentOfName('QS-Form', this);
						if (QSForm?.getRequireds && QSForm.getRequireds.find(i => i == this.prop))
							return true;
					}
					if (!this.rules || typeof this.rules != 'object') return false;
					if (isArray(this.rules)) {
						return -1 != this.rules.findIndex(i => i.required)
					} else {
						return this.rules.required;
					}
				}
			},
			mounted() {
				// #ifdef MP-ALIPAY
				if (this.dynamicForm) return;
				// #endif
				this.setInParent();
			},
			beforeDestroy() {
				// #ifdef MP-ALIPAY
				if (this.dynamicForm) return;
				// #endif
				this.removeInParent();
			},
			methods: {
				setFormItem(formItem) {
					if(!els[this.componentId]) els[this.componentId] = {};
					els[this.componentId].formItem = formItem;
				},
				setValidateState(obj) {
					const QSFormItem = getParentOfName('QS-Form-Item', this) || els[this.componentId].formItem;
					if (QSFormItem?.setValidateState && typeof QSFormItem?.setValidateState ==
						'function') {
						QSFormItem.setValidateState(obj)
					}
				},
				setInParent() {
					const QSForm = getParentOfName('QS-Form', this);
					if (QSForm?.setItem && typeof QSForm?.setItem == 'function') {
						QSForm.setItem(this);
					} else {
						console.log('找不到form')
						console.warn('找不到form')
					}

					const QSFormItem = getParentOfName('QS-Form-Item', this);
					if (QSFormItem?.setEl && typeof QSFormItem?.setEl == 'function') {
						QSFormItem.setEl(this);
					} else {
						console.warn('找不到form-item')
					}
				},
				removeInParent() {
					const QSForm = getParentOfName('QS-Form', this);
					if (QSForm?.removeItem && typeof QSForm?.removeItem == 'function') {
						QSForm.removeItem(this);
					} else {
						console.log('找不到form')
						console.warn('找不到form')
					}

					const QSFormItem = getParentOfName('QS-Form-Item', this);
					if (QSFormItem?.removeEl && typeof QSFormItem?.removeEl == 'function') {
						QSFormItem.removeEl(this);
					} else {
						console.warn('找不到form-item')
					}
				},
				getValue() {
					return this.val;
				},
				getLabel() {
					const QSFormItem = getParentOfName('QS-Form-Item', this) || els[this.componentId].formItem;
					return this?.label || QSFormItem?.label || '';
				},
				getRules() {
					let thisRule;
					if (this.rules && typeof this.rules == 'object') {
						if (isArray(this.rules)) {
							thisRule = this.rules;
						} else {
							thisRule = [this.rules];
						}
					}
					let QSFormItemRule;
					const QSFormItem = getParentOfName('QS-Form-Item', this) || els[this.componentId].formItem;
					if (QSFormItem && QSFormItem?.rules && typeof QSFormItem?.rules == 'object') {
						if (isArray(QSFormItem.rules)) {
							QSFormItemRule = QSFormItem.rules;
						} else {
							QSFormItemRule = [QSFormItem.rules];
						}
					}
					if (!thisRule && !QSFormItemRule) return null;
					if (!thisRule && QSFormItemRule) return QSFormItemRule;
					if (thisRule && !QSFormItemRule) return thisRule;
					if (thisRule && QSFormItemRule) return [...thisRule, ...QSFormItemRule];
				},
				change(e) {
					let val;
					switch (componentType) {
						default:
							val = e.detail.value;
							break;
					}
					this.val = val;
					this.$emit('change', val);
					this.$emit('input', val);
				}
			}
		}
	}
}
