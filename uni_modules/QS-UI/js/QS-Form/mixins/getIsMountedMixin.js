module.exports = function () {
	return {
		data() {
			return {
				// #ifdef MP-TOUTIAO || MP-ALIPAY || MP-BAIDU
				isMounted: false,
				// #endif
				// #ifndef MP-TOUTIAO || MP-ALIPAY || MP-BAIDU
				isMounted: true,
				// #endif
			}
		},
		mounted() {
			this.isMounted = true;
		}
	}
}