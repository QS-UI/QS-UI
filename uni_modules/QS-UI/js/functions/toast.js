import mergeArg from './mergeArg.js';
let defaultConfig;
try {
	defaultConfig = require('@/QS-UI-CONFIG/js/functions/showToast/defaultConfig.js');
} catch (e) {
	//TODO handle the exception
	defaultConfig = () => {
		return {
		title: '',
		duration: 1500,
		mask: false,
		icon: 'none',
		image: '',
		position: 'center',
		success: () => {},
		fail: () => {},
		complete: () => {}
	}
	}
}
const showToast = uni.showToast;	//保留原有的uni.showModal
module.exports = function() {
	const config = mergeArg(defaultConfig(), arguments)
	config.title = String(config.title)
	showToast(config)
}