/**
 * 预览图片，如果urls字段是字符串，相当于一张图片地址，则自动转数组
 */
import mergeArg from './mergeArg.js';
import { isArray } from '../baseUtil.js';

let defaultConfig;
try {
	defaultConfig = require('@/QS-UI-CONFIG/js/functions/previewImage/defaultConfig.js');
} catch (e) {
	//TODO handle the exception
	defaultConfig = () => {
		return {
			urls: [], 
			current: 0, 
			indicator: 'default', 
			loop: false, 
			longPressActions: null, 
			success: ()=>{}, 
			fail: ()=>{}, 
			complete: ()=>{} 
		}
	}
}
const previewImage = uni.previewImage;	//保留原有的uni.previewImage

module.exports = function () {
	const obj = mergeArg(defaultConfig(), arguments);
	if(!isArray(obj.urls)) obj.urls = [obj.urls];
	previewImage(obj)
}