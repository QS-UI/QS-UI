import mergeArg from './mergeArg.js';
let defaultConfig;
try {
	defaultConfig = require('@/QS-UI-CONFIG/js/functions/showModal/defaultConfig.js');
} catch (e) {
	//TODO handle the exception
	defaultConfig = () => {
		return {
			content: '',
			title: '',
			showCancel: true,
			cancelText: '取消',
			cancelColor: '',
			confirmText: '确定',
			confirmColor: '',
			success: () => {},
			fail: () => {},
			complete: () => {}
		}
	}
}
const showModal = uni.showModal;	//保留原有的uni.showModal
module.exports = function() {
	const config = mergeArg(defaultConfig(), arguments)
	showModal(config);
}
