module.exports = function(name = 'QS-Form', vm) {
	const target = vm[name.replace(/-/g, '')];
	if(target){ 
		if(typeof target == 'function') return target();
		return target;
	}
	let parent = vm.$parent;
	let parentName = parent.$options.name;
	while (parentName !== name) {
		parent = parent.$parent;
		if (!parent) return false
		parentName = parent.$options.name;
	}
	return parent;
}
