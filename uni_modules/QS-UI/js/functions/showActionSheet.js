import mergeArg from './mergeArg.js';

let defaultConfig;
try {
	defaultConfig = require('@/QS-UI-CONFIG/js/functions/showActionSheet/defaultConfig.js');
} catch (e) {
	//TODO handle the exception
	defaultConfig = () => {
		return {
			itemList: [],
			itemColor: '#000000',
			popover: null,
			success: () => {},
			fail: () => {},
			complete: () => {}
		}
	}
}
const showActionSheet = uni.showActionSheet;	//保留原有的uni.showActionSheet

module.exports = function () {
	const config = mergeArg(defaultConfig(), arguments)
	showActionSheet(config)
}