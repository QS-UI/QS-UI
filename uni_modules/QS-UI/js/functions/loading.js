import mergeArg from './mergeArg.js';
let defaultConfig;
try {
	defaultConfig = require('@/QS-UI-CONFIG/js/functions/loading/defaultConfig.js');
} catch (e) {
	//TODO handle the exception
	defaultConfig = () => {
		return {
			title: '请稍候',
			mask: false,
			success: () => {},
			fail: () => {}
		}
	}
}
const loading = uni.showLoading;	//保留原有的uni.showLoading
const closeLoading = uni.hideLoading;	//保留原有的uni.showLoading
const hideLoading = function() {
	closeLoading()
}

const showLoading = function() {
	const config = mergeArg(defaultConfig(), arguments);
	config.title = String(config.title)
	loading(config)
}
module.exports = {
	hideLoading,
	showLoading
}