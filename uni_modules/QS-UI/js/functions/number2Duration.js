/**
 * 数值转中文时间, 摘自网上
 * @param {Number} my_time 时间值
 */

import mergeArg from './mergeArg.js';
const timesFieldsObj = {
	day: {
		field: 1,
		mulTime: 1000*60*60*24,
		divTime: 1000/60/60/24,
		unit: '天'
	},
	hour: {
		field: 24,
		mulTime: 1000*60*60,
		divTime: 1000/60/60,
		unit: '时'
	},
	minute: {
		field: 60,
		mulTime: 1000*60,
		divTime: 1000/60,
		unit: '分'
	},
	second: {
		field: 60,
		mulTime: 1000,
		divTime: 1000,
		unit: '秒'
	}
}
const timesFieldsArr = Object.keys(timesFieldsObj).map(i=>({ fields: i, ...timesFieldsObj[i] }));

module.exports = function() {
	let { time, fields } = mergeArg({ time: 0, fields: 'day' }, arguments);
	let obj = {
		day: '', 
		hour: '', 
		minute: '', 
		second: ''
	};
	const index = timesFieldsArr.findIndex(i=>i.fields == fields);
	if(-1 == index) {
		console.log('number2Duration.js粒度不存在');
		return;
	}
	time = +new Date(time);
	let arr = [];
	let text = '';
	for(let i = index; i < timesFieldsArr.length; i++) {
		const item = timesFieldsArr[i];
		let data = Math.floor(time / item.mulTime);
		if(fields == 'day') {
			if(obj.minute) data -= 24 * 60 * 60 * Number(obj.day) + 60 * 60 * Number(obj.hour) + 60 * Number(obj.minute);
			else if(obj.hour) data -= 24 * 60 * Number(obj.day) + 60 * Number(obj.hour);
			else if(obj.day) data -= 24 * Number(obj.day);
		}else if(fields == 'hour') {
			if(obj.minute) {
				data -= 60 * 60 * Number(obj.hour) + 60 * Number(obj.minute)
			}
			else if(obj.hour) {
				data -= 60 * Number(obj.hour)
			};
		}else if(fields == 'minute') {
			if(obj.minute) data -= 60 * Number(obj.minute);
		}
		data = Math.floor(data)
		data = data > 9?(''+data):('0' + data);
		obj[item.fields] = data;
		arr.push({ data: data, field: item.fields, index: i });
		text += data + item.unit;
	}
	return { text: text, ...obj };
}
