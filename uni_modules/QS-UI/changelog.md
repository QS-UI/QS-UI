## 1.0.9（2021-05-29）
* #### 修复 修复更新后一些报错问题
## 1.0.8（2021-05-29）
* #### 新增 `QS-DynamicForm` 动态表单组件
* #### 新增 `QS-SuperComponent` 根据json渲染ui的组件
* #### 新增 `QS-Canvas` 基于QS-SharePoster的画布组件, 暂时只支持APP-VUE、H5、微信小程序
* #### 新增 QS组件新增`emitQuery模式`，可在组件上传`autoEmitQuery`为true, 并监听`@emitQuery`事件在组件mounted后触发该事件并返回布局信息
* #### 新增 QS-UI-CONFIG/js新增mixin目录, 并在mixin目录下新增`QS-Components-Mixin-defProps.js` 用于覆盖QS-Components-Mixin.js的默认参数(`emitQuery`相关参数可在这里配置)
* #### 新增 tabbar新增模板页面并增加`home-1模板`
* #### 新增 `QS-Radio`
* #### 新增 `QS-Checkbox`
* #### 新增 `QS-Switch`
* #### 新增 `QS-Grids` 宫格组件
* #### 新增 JSAPI新增`intersectionObserver`布局相交监听器
* #### 新增 JSAPI新增`convertToPinyin`中文转拼音
* #### 优化 组件内可覆盖props配置 均使用require
* #### 优化 `QS-Tabs`实际item会影响布局导致line位置不准确问题, 并在mounted后两次获取布局信息以增加布局信息准确性
* #### 优化 JSAPI优化`countDown.js、number2Duration.js` 支持粒度配置, 并优化返回数据详细信息
* #### 优化 `QS-Sticky`
* #### 优化 小细节优化
* #### 修复 `QS-SuperComponent`组件在支付宝小程序无效问题
## 1.0.7（2021-04-21）
* #### 新增 `QS-Form`、 `QS-Form-Item`、 `QS-Input` 组件, 自定义样式与校验的表单，目前为大致模型，下版本完善，有建议抓紧提
## 1.0.6（2021-04-10）
* #### 修复 默认QS-UI-CONFIG报错问题
## 1.0.5（2021-04-08）
* #### 新增 示例新增js-Query-布局信息
* #### 新增 示例新增mixin-Refresh
* #### 新增 uni.$qs.v 版本号
* #### 新增 文档新增体验二维码
* #### 优化 优化请求在QS-UI-CONFIG->config->request.js 中 setConfig、checkRes都支持返回Promise, 意味着你可以做一些异步操作, 比如判断未登录 进行登录后 继续执行 request
## 1.0.4（2021-03-30）
* #### 修复 iOS Nvue QS-BackTop 返回顶部问题, Nvue QS-BackTop 需放在最前面
* #### 修复 字节跳动小程序 QS-Tabs line 背景色块不显示问题
## 1.0.3（2021-03-30）
* #### 修复 vue页面使用QS-NodeNav parent 问题 导致 交互反馈示例页 QS-NodeNav组件不显示问题
## 1.0.2（2021-03-30）
* #### 优化 目前功能兼容nvue, 并且以此方向开发
* #### 修复 百度小程序某些问题
## 1.0.1（2021-03-21）
* #### 示例项目新增QS组件须知页面
* #### 示例组件微调
* #### 更新文档
## 1.0.0（2021-03-19）
1.0.0
