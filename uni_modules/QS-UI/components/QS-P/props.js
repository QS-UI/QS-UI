module.exports = {
	height: {
		type: [Number, String],
		default: '12.5px'
	},
	width: {
		type: [Number, String],
		default: '12.5px'
	},
	size: {
		type: [Number, String],
		default: ''
	},
	borderRadius: {
		type: [Number, String],
		default: ''
	},
	backgroundColor: {
		type: String,
		default: ''
	},
}