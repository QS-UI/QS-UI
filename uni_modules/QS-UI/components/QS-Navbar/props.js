module.exports = {
	fixed: {
		type: [Boolean, String],
		default: true
	},
	backgroundColor: {
		type: String,
		default: 'none'
	},
	statusBar: {
		type: [Boolean, String],
		default: true
	},
	navbarItems: {
		type: Array,
		default () {
			return [];
		}
	},
	diffMenuButtonInfo: {
		type: [Boolean, String],
		default: false
	},
	hasPlacherholder: {
		type: [Boolean, String],
		default: true
	},
	zIndex: {
		type: [Number, String],
		default: 998
	},
	relaunchPath: {
		type: String,
		default: ''
	},
}