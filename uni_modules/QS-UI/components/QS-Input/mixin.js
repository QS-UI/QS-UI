import classObj2String from '../../js/functions/classObj2String';
import styleObj2String from '../../js/functions/styleObj2String';

import getninputStyle from '../../js/QS-Form/js/styles/input.js';

module.exports = function () {
	let ninputStyle;
	// #ifdef APP-NVUE
	ninputStyle = getninputStyle(true);
	// #endif
	// #ifndef APP-NVUE
	ninputStyle = getninputStyle();
	// #endif
	const props = {
		placeholderStyle: {
			type: String,
			default: ''
		},
		placeholderClass: {
			type: String,
			default: 'input-placeholder'
		},
		maxlength: {
			type: [String,Number],
			default: 140
		},
		cursorSpacing: {
			type: [String,Number],
			default: 0
		},
		confirmType: {
			type: String,
			default: 'done'
		},
		confirmHold: {
			type: Boolean,
			default: false
		},
		selectionStart: {
			type: [String,Number],
			default: -1
		},
		selectionEnd: {
			type: [String,Number],
			default: -1
		},
		cursor: {
			type: [String,Number],
			default: ''
		},
		adjustPosition: {
			type: [Boolean, String],
			default: true
		},
		type: {
			type: String,
			default: 'text'
		},
		focus: {
			type: Boolean,
			default: false
		},
		password: {
			type: Boolean,
			default: false
		},
		disabled: {
			type: Boolean,
			default: false
		},
		placeholder: {
			type: String,
			default: ''
		},
		
		// textarea
		autoHeight: {
			type: Boolean,
			default: false
		}
	}
	
	return {
		props,
		mixin: {
			// #ifndef MP-ALIPAY
			props,
			// #endif
			data() {
				return {
					nFocus: this.focus
				}
			},
			computed: {
				getDisabled() {
					return this.disabled;
				},
				getMaxlength() {
					return Number(this.maxlength);
				},
				getCursorSpacing() {
					return Number(this.cursorSpacing);
				},
				getSelectionStart() {
					return Number(this.selectionStart);
				},
				getSelectionEnd() {
					return Number(this.selectionEnd);
				},
				getCursor() {
					return Number(this.cursor);
				},
				getAdjustPosition() {
					return Boolean(this.adjustPosition);
				},
				getAutoHeight() {
					return Boolean(this.autoHeight);
				},
				getCompClass_input() {
					// #ifndef APP-NVUE
					return classObj2String(this.compClass.input);
					// #endif
					// #ifdef APP-NVUE
					return [classObj2String(this.compClass.input)];
					// #endif
				},
				QS_nCompStyle() {
					let s = '';
					s += `;${styleObj2String(ninputStyle.container)};`;
					if(this.nFocus) {
						s += `;${styleObj2String(ninputStyle.containerFocus)};`;
					}
					return s;
				},
				getCompStyle_input() {
					let s = '';
					let height = (this.getFontSize + 12) + 'px';
					s += `;font-size:${this.getFontSize}px;height:${height};line-height:${height};`;
					s += `;${styleObj2String(ninputStyle.input)};${styleObj2String(this.compStyle.input)};`;
					if(this.nFocus) {
						s += `;${styleObj2String(ninputStyle.inputFocus)};${styleObj2String(this.compStyle.inputFocus)}`;
					}
					return s;
				}
			},
			methods: {
				setFocus(bl) {
					this.nFocus = bl;
				},
				blurFn(e) {
					this.setFocus(false);
					this.$emit('blur', e);
				},
				focusFn(e) {
					this.setFocus(true);
					this.$emit('focus', e);
				}
			}
		}
	}
}