module.exports = {
	width: {
		type: [String, Number],
		default: '750rpx'
	},
	height: {
		type: [String, Number],
		default: ''
	},
	tabs: {
		type: Array,
		default: () => []
	},
	space: {
		type: [String, Number],
		default: ''
	},
	autoScrollInto: {
		type: [String, Boolean],
		default: true
	},
	activeFontSize: {
		type: [String, Number],
		default: ''
	},
	defFontSize: {
		type: [String, Number],
		default: ''
	},
	activeFontBold: {
		type: [String, Boolean],
		default: true
	},
	activeColor: {
		type: String,
		default: ''
	},
	defColor: {
		type: String,
		default: '#999999'
	},
	tabIndex: {
		type: [String, Number],
		default: 0
	},
	props: {
		type: Object,
		default: () => {
			return {}
		}
	},
	type: {
		type: String,
		default: ''
	},
	backgroundColor: {
		type: String,
		default: ''
	},
	mode: {
		type: String,
		default: 'auto'
	},
	hasLine: {
		type: Boolean,
		default: true
	},
	lineType: {
		type: String,
		default: ''
	},
	lineUseSlot: {
		type: Boolean,
		default: false
	},
	lineColor: {
		type: String,
		default: ''
	},
	theme: {
		type: String,
		default: 'default'
	},
	itemWidth: {
		type: String,
		default: ''
	},
	itemMinWidth: {
		type: String,
		default: ''
	},
	itemMaxWidth: {
		type: String,
		default: ''
	},
	// #ifndef APP-NVUE
	itemFull: {
		type: Boolean,
		default: true
	},
	// #endif
	// #ifdef APP-NVUE
	itemFull: {
		type: Boolean,
		default: false
	},
	// #endif
}