module.exports = {
	gridMinHeight: {
		type: [String, Number],
		default: 0,
		des: `单个格子最小高度`
	},
	badgeRight: {
		type: [Number, String],
		default: '',
	},
	badgeTop: {
		type: [Number, String],
		default: '',
	},
	badgeLeft: {
		type: [Number, String],
		default: '',
	},
	badgeBottom: {
		type: [Number, String],
		default: '',
	},
	row: {
		type: [Number, String],
		default: 5,
		des: `单行个数`
	},
	col: {
		type: [Number, String],
		default: 2,
		des: `行数, mode为swiper时生效`
	},
	paddingRow: {
		type: [Number, String],
		default: 0,
		des: `横向内边距`
	},
	paddingCol: {
		type: [Number, String],
		default: 0,
		des: `纵向内边距`
	},
	iconSizeScale: {
		type: [Number, String],
		default: .8,
		des: `图标缩放系数`
	},
	gridTextColor: {
		type: String,
		default: '#000000',
		des: `文字颜色`
	},
	gridTextSize: {
		type: String,
		default: '28rpx',
		des: `文字大小`
	},
	gridTextMarginTop: {
		type: [Number, String],
		default: '18rpx',
		des: `文字距离图片的距离`
	},
	mode: {
		type: String,
		default: '',
		values: ['', 'swiper'],
		des: `展示模式`
	},
	props: {
		type: Object,
		default() { return {} },
		des: `字段配置`
	},
	gridOutline: {
		type: String,
		default: 'none',
		des: `css-outline属性`
	},
	gridPadding: {
		type: [Number, String],
		default: '15rpx',
		des: `单个格子内边距`
	},
	iconBorderRadius: {
		type: [Number, String],
		default: '0',
		des: `图片圆角值`
	},
	
	
	indicatorDots: {
		type: [Boolean, String],
		default: false
	},
	indicatorDotsHieght: {
		type: [Number, String],
		default: 0
	},
	duration: {
		type: [Number, String],
		default: 500
	},
	circular: {
		type: [String, Boolean],
		default: false
	},
	defCurrent: {
		type: [String, Number],
		default: 0,
		des: `mode为swiper时默认current值`
	},
	previousMargin: {
		type: String,
		default: '0px'
	},
	nextMargin: {
		type: String,
		default: '0px'
	},
	indicatorColor: {
		type: String,
		default: 'rgba(0, 0, 0, .3)'
	},
	indicatorActiveColor: {
		type: String,
		default: '#000000'
	},
	autoplay: {
		type: [String, Boolean],
		default: false
	},
	interval: {
		type: [String, Number],
		default: 5000
	},
	gridsData: {
		type: Array,
		default: ()=>[],
		des: `宫格数据`
	},
	hideText: {
		type: [String, Boolean],
		default: false,
		des: `是否隐藏文字`
	},
	iconWidth: {
		type: [String, Number],
		default: 0,
		des: `图片宽度`
	},
	iconHeight: {
		type: [String, Number],
		default: 0,
		des: `图片高度`
	},
	gridPaddingTop: {
		type: [Number, String],
		default: 0
	},
	gridPaddingBottom: {
		type: [Number, String],
		default: 0
	},
	gridPaddingLeft: {
		type: [Number, String],
		default: 0
	},
	gridPaddingRight: {
		type: [Number, String],
		default: 0
	},
	ellipsis: {
		type: [String, Boolean],
		default: false
	},
	useQSAnimation: {
		type: [String, Boolean],
		default: false
	},
	animationType: {
		type: String,
		default: 'jump'
	},
	iconMinHeight: {
		type: String,
		default: '',
		des: `图片最小高度`
	},
}