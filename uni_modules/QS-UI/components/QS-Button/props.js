module.exports = {
	waves: {
		type: [String, Boolean],
		default: true
	},
	txt: {
		type: String,
		default: ''
	},
	wavesColor: {
		type: String,
		default: ''
	},
	theme: {
		type: String,
		default: 'default'
	},
	size: {
		type: String,
		default: 'default'
	},
	disabled: {
		type: Boolean,
		default: false
	},
	loading: {
		type: Boolean,
		default: false
	},
	plain: {
		type: Boolean,
		default: false
	},
	formType: {
		type: String,
		default: ''
	},
	openType: {
		type: String,
		default: ''
	},
	hoverStartTime: {
		type: Number,
		default: 20
	},
	hoverStayTime: {
		type: Number,
		default: 70
	},
	hoverClass: {
		type: String,
		default: ''
	},
	preIconType: {
		type: String,
		default: ''
	},
	preIconColor: {
		type: String,
		default: ''
	},
	setTimeoutClick: {
		type: [String, Boolean],
		default: false
	},
	iconAnimationType: {
		type: String,
		default: ''
	},
	height: {
		type: [String, Number],
		default: ''
	},
}